package main

import (
	"context"
	"gitlab.com/bat.ggl/pubsub/internal/application"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/config"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/schema"
	"gitlab.com/bat.ggl/pubsub/internal/interfaces"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	ctx := context.Background()

	stopping := make(chan os.Signal)
	log, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	sugar := log.Sugar()

	config, err := config.NewConfig()
	if err != nil {
		sugar.Fatal(err)
	}

	config.Schema = schema.LoadSchema(sugar, config.SchemaPath)
	app := application.NewApplication(config, sugar, ctx)
	go shutdownMonitor(stopping, app, ctx)
	interfaces.RegisterCli(app)
}

func shutdownMonitor(stopping chan os.Signal, app *application.Application, ctx context.Context) {
	signal.Notify(stopping, os.Interrupt, syscall.SIGTERM)
	for {
		for range stopping {
			app.Shutdown()
			ctx.Done()
		}
	}
}
