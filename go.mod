module gitlab.com/bat.ggl/pubsub

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible // indirect
	github.com/go-errors/errors v1.1.1
	github.com/go-pg/pg v8.0.7+incompatible // indirect
	github.com/go-playground/validator/v10 v10.3.0
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/prometheus/client_golang v1.7.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	go.uber.org/zap v1.16.0
	mellium.im/sasl v0.2.1 // indirect
)
