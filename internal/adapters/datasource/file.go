package datasource

import (
	"bufio"
	"encoding/json"
	"errors"
	"gitlab.com/bat.ggl/pubsub/internal/domain/entity"
	"os"
)

type FileStorage struct {
	filePath string
}

func CreateFileStorage(path string) *FileStorage {
	return &FileStorage{
		filePath: path,
	}
}

// todo - тут надо открыть файл для записи,
// и проверить что он доступен, перед тем как начать писать.
// в env надо добавить возможность указывать куда и в какой файл надо писать.
func (f *FileStorage) Save(row entity.AbstractRow) error {

	file, err := os.OpenFile(f.filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	datawriter := bufio.NewWriter(file)

	data, _ := json.Marshal(row)
	if err != nil {
		return errors.New("failed to save data")
	}
	_, _ = datawriter.WriteString(string(data) + "\n")

	datawriter.Flush()

	return nil
}
