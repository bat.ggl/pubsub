package datasource

import (
	"gitlab.com/bat.ggl/pubsub/internal/domain/entity"
	"go.uber.org/zap"
)

type StdOutStorage struct {
	logger *zap.SugaredLogger
}

func CreateStdOutStorage(logger *zap.SugaredLogger) *StdOutStorage {
	return &StdOutStorage{
		logger: logger,
	}
}

func (f *StdOutStorage) Save(row entity.AbstractRow) error {
	f.logger.Infof("ID:%s, value: %d", row.ID, row.Value)
	return nil
}
