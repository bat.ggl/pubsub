package datasource

import (
	"gitlab.com/bat.ggl/pubsub/internal/domain/repository"
	"go.uber.org/zap"
)

type AbstractStorage struct {
	fileStorage   *FileStorage
	stdOutStorage *StdOutStorage
}

type StorageConfig struct {
	FilePath string
}

func CreateStorage(logger *zap.SugaredLogger, config StorageConfig) *AbstractStorage {
	return &AbstractStorage{
		fileStorage:   CreateFileStorage(config.FilePath),
		stdOutStorage: CreateStdOutStorage(logger),
	}
}

func (s *AbstractStorage) GetStorage(c int) repository.AbstractStorage {
	switch c {
	case 0:
		return s.stdOutStorage
	case 1:
		return s.fileStorage
	default:
		return s.fileStorage
	}
}
