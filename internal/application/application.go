package application

import (
	"context"
	"github.com/go-errors/errors"
	"gitlab.com/bat.ggl/pubsub/internal/adapters/datasource"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/agreagators"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/config"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/generator"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/queue"
	"go.uber.org/zap"
	"hash/fnv"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Application struct {
	config *config.Config
	logger *zap.SugaredLogger
	broker *queue.BrokerMessage
	ctx    context.Context
}

func NewApplication(config *config.Config, logger *zap.SugaredLogger, ctx context.Context) *Application {
	return &Application{
		config: config,
		logger: logger,
		ctx:    ctx,
	}
}

func (a *Application) Run(filepath string) {
	wg := sync.WaitGroup{}

	storages := datasource.CreateStorage(a.logger, datasource.StorageConfig{
		FilePath: filepath,
	})

	storage := storages.GetStorage(a.config.Schema.AbstractStorageType)

	var err error
	a.broker, err = queue.CreateBrokerMessage(a.logger, a.config.Schema.Queue.Size)
	if err != nil {
		a.logger.Fatal(err.Error())
	}

	a.broker.DeclareExchange(generator.ExchangeName)

	generator.RunGenerators(a.logger, a.ctx, a.config.Schema.Generators, a.broker)

	for index, value := range a.config.Schema.Aggregators {

		hash, err := generateAggregatorHash(value.SubIds, index)
		if err != nil {
			a.logger.Error(err)
		}

		a.broker.DeclareQueue(queue.RegisterQueue{
			Name: string(hash),
			Size: a.config.Schema.Queue.Size,
		})

		if err := a.broker.Binding(queue.Binding{
			QueueName:    string(hash),
			RoutingKey:   value.SubIds,
			ExchangeName: generator.ExchangeName,
		}); err != nil {
			a.logger.Error("new Aggregator - bind err")
		}
		agg := agreagators.NewAggregator(
			a.logger,
			hash,
			value.SubIds,
			time.Duration(value.AggregatePeriod),
			a.broker,
			storage,
			&wg,
		)
		wg.Add(1)
		go agg.Start()
	}

	wg.Wait()
}

func (a *Application) Shutdown() {
	a.broker.Shutdown()
}

func generateAggregatorHash(ids []string, index int) (uint32, error) {
	words := strings.Join(ids, "_")

	h := fnv.New32a()
	_, err := h.Write([]byte(words + strconv.Itoa(index)))
	if err != nil {
		return 0, errors.Errorf("failed to run aggregator: %s", err.Error())
	}

	return h.Sum32(), nil
}
