package entity

type AbstractRow struct {
	ID    string
	Value int64
}
