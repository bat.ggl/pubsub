package repository

import "gitlab.com/bat.ggl/pubsub/internal/domain/entity"

type AbstractStorage interface {
	Save(row entity.AbstractRow) error
}
