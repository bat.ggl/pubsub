package agreagators

import (
	"gitlab.com/bat.ggl/pubsub/internal/domain/entity"
	"gitlab.com/bat.ggl/pubsub/internal/domain/repository"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/queue"
	"go.uber.org/zap"
	"sync"
	"time"
)

type Aggregator struct {
	index            uint32                     // aggreagator hash name
	subIds           []string                   // exchange name
	aggregatePeriodS time.Duration              // period load data
	queue            *queue.BrokerMessage       // queue object
	storage          repository.AbstractStorage // storage
	wg               *sync.WaitGroup            // wg sync
	logger           *zap.SugaredLogger
}

func NewAggregator(
	logger *zap.SugaredLogger,
	hash uint32,
	IDs []string,
	period time.Duration,
	queue *queue.BrokerMessage,
	storage repository.AbstractStorage,
	wg *sync.WaitGroup,
) *Aggregator {
	return &Aggregator{
		logger:           logger,
		index:            hash,
		subIds:           IDs,
		aggregatePeriodS: period,
		queue:            queue,
		storage:          storage,
		wg:               wg,
	}
}

func (a *Aggregator) collect() {
	t := time.NewTicker(a.aggregatePeriodS * time.Second)
	defer a.Shutdown()
	batch := make([]interface{}, 0)
	for range t.C {
		for {
			data, err := a.queue.Pop(string(a.index))
			if err == queue.ErrShutdownBroker {
				return
			}

			if data == nil {
				break
			}
			batch = append(batch, data)
		}

		go func() {
			average := a.calculate(batch)
			a.send(average)
		}()

	}
}

func (a *Aggregator) calculate(batch []interface{}) map[string]int64 {

	groups := make(map[string][]int64)

	for _, item := range batch {
		switch elem := item.(type) {
		case entity.AbstractRow:
			_, ok := groups[elem.ID]
			if ok {
				groups[elem.ID] = append(groups[elem.ID], elem.Value)
			} else {
				groups[elem.ID] = append(groups[elem.ID], elem.Value)
			}
		default:
			a.logger.Errorf("faled to define type of data")
		}
	}

	result := make(map[string]int64)
	for name, group := range groups {
		size := len(group)
		var sum int64
		for _, numb := range group {
			sum += numb
		}
		if size == 0 {
			a.logger.Error("failed to calculate data")
			break
		}

		average := sum / int64(size)
		result[name] = average
	}

	return result
}

func (a *Aggregator) send(batch map[string]int64) {
	for name, value := range batch {
		row := entity.AbstractRow{
			ID:    name,
			Value: value,
		}

		if err := a.storage.Save(row); err != nil {
			a.logger.Errorf("failed to save average")
		}
	}

}

func (a *Aggregator) Start() {
	a.collect()
}

func (a *Aggregator) Shutdown() {
	a.logger.Infof("Shutdown aggregator")
	a.wg.Done()
}
