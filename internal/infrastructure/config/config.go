package config

import (
	"github.com/joho/godotenv"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/schema"
	"log"
	"os"
	"strconv"
)

type Config struct {
	IsProd     bool
	SchemaPath string
	Schema     *schema.Schema
}

func NewConfig() (*Config, error) {
	config := &Config{}
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	config.SchemaPath = os.Getenv("CONFIG_PATH")
	var err error
	config.IsProd, err = strconv.ParseBool(os.Getenv("IS_PROD"))
	if err != nil {
		config.IsProd = true
	}

	return config, nil
}
