package generator

import (
	"context"
	"errors"
	"gitlab.com/bat.ggl/pubsub/internal/domain/entity"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/queue"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/schema"
	"go.uber.org/zap"
	"math/rand"
	"time"
)

type generator struct {
	logger      *zap.SugaredLogger
	TimeOut     time.Duration
	SendPeriod  time.Duration
	DataSources []schema.Sources
	end         chan bool
	broker      *queue.BrokerMessage
	ctx         context.Context
	poolNotify  chan bool
}

type DataSources struct {
	ID            string
	InitValue     int
	MaxChangeStep int
}

const ExchangeName = "pubSub"

func NewGenerator(logger *zap.SugaredLogger,
	done chan bool,
	timeOut time.Duration,
	sendPeriod time.Duration,
	dataSources []schema.Sources,
	broker *queue.BrokerMessage,
	ctx context.Context,
) *generator {
	return &generator{
		logger:      logger,
		TimeOut:     timeOut,
		SendPeriod:  sendPeriod,
		DataSources: dataSources,
		end:         make(chan bool),
		poolNotify:  done,
		broker:      broker,
		ctx:         ctx,
	}
}

func (g *generator) Run() {
	t := time.NewTicker(g.SendPeriod * time.Second)
	go g.StopByTimeout(g.TimeOut)

	for {
		select {
		case <-t.C:
			for index, value := range g.DataSources {
				number := value.InitValue + rand.Intn(value.MaxChangeStep)
				msg := queue.Message{
					ExchangeName: ExchangeName,
					RoutingKey:   value.Id,
					Data:         entity.AbstractRow{ID: value.Id, Value: int64(number)},
				}

				if err := g.broker.Push(msg); err != nil {
					if errors.Is(err, queue.ErrShutdownBroker) {
						return
					}
					g.logger.Errorf("failed push to queue: %s", err.Error())
					return
				}

				g.DataSources[index].InitValue = number
			}
		case <-g.end:
			t.Stop()
			g.poolNotify <- true
			g.logger.Infof("Shutdown generator by timer")
			return
		case <-g.ctx.Done():
			t.Stop()
			g.poolNotify <- true
			g.logger.Infof("Shutdown generator by gracefully")
			return
		}
	}
}

func (g *generator) StopByTimeout(timeOut time.Duration) {
	t := time.NewTicker(time.Second * timeOut)
	for range t.C {
		g.end <- true
		break
	}
}
