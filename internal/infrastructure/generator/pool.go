package generator

import (
	"context"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/queue"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/schema"
	"go.uber.org/zap"
	"sync/atomic"
	"time"
)

type PoolOfGenerators struct {
	logger  *zap.SugaredLogger
	done    chan bool // chan when worker don job
	counter int64     // counter of workers
	broker  *queue.BrokerMessage
}

func (p *PoolOfGenerators) Watch() {
	for {
		<-p.done
		atomic.AddInt64(&p.counter, -1)
		if p.counter == 0 {
			p.broker.Shutdown()
			p.logger.Info("broker shutdown")
		}
	}
}

// run all generators
func RunGenerators(logger *zap.SugaredLogger, ctx context.Context, generators []schema.Generator, broker *queue.BrokerMessage) {
	done := make(chan bool, len(generators))
	pool := &PoolOfGenerators{
		done:    done,
		counter: 0,
		broker:  broker,
		logger:  logger,
	}

	for _, g := range generators {
		gen := NewGenerator(
			logger,
			done,
			time.Duration(g.TimeoutInSec),
			time.Duration(g.SendPeriodSec),
			g.DataSources,
			broker,
			ctx,
		)
		atomic.AddInt64(&pool.counter, 1)
		go gen.Run()
	}

	go pool.Watch()
}
