package queue

// ===============================  EXCHANGE  =============================================
type exchange struct {
	exchangeName string
	routing      routing
}

type routing struct {
	keys map[string]string
}

type Binding struct {
	QueueName    string   // name of the queue
	RoutingKey   []string // name of key for route
	ExchangeName string   // name of the exchange
}

// create new exchange
func createExchange(name string) *exchange {
	return &exchange{
		exchangeName: name,
		routing:      routing{keys: map[string]string{}},
	}
}
