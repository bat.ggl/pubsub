package queue

import (
	"errors"
	"go.uber.org/zap"
)

var (
	ErrExchangeIsNotExists  = errors.New("failed to send message to exchange, exchange is not exists")
	ErrExchangeNoEnoughSize = errors.New("failed to send message to exchange, exchange is full")
	ErrExchangeSizeZero     = errors.New("failed to create message to exchange, exchange size can't be 0")
	ErrShutdownBroker       = errors.New("failed to push message, broker shutdown")
)

// ===============================  QUEUE  =============================================
type BrokerMessage struct {
	logger         *zap.SugaredLogger
	exchangesPool  map[string]*exchange
	countOfMessage uint64
	queuePool      map[string]*queue
	shutdown       bool
}

func CreateBrokerMessage(logger *zap.SugaredLogger, size uint64) (*BrokerMessage, error) {
	if size == 0 {
		return nil, ErrExchangeSizeZero
	}

	return &BrokerMessage{
		logger:         logger,
		exchangesPool:  map[string]*exchange{},
		countOfMessage: 0,
		queuePool:      map[string]*queue{},
		shutdown:       false,
	}, nil
}

// создаем обменник для очереди
func (q *BrokerMessage) DeclareExchange(name string) {
	q.exchangesPool[name] = createExchange(name)
}

func (q *BrokerMessage) Binding(bind Binding) error {
	exchange, ok := q.exchangesPool[bind.ExchangeName]
	if !ok {
		return errors.New("can't binding exchange & queue. Exchange don't exists")
	}

	if _, ok := q.queuePool[bind.QueueName]; !ok {
		return errors.New("can't binding exchange & queue. Queue don't exists")
	}

	for _, value := range bind.RoutingKey {
		exchange.routing.keys[value] = bind.QueueName
	}

	return nil
}

// Public structure for work with queue
type Message struct {
	ExchangeName string      // key of the exchange
	RoutingKey   string      // key for routing to queue
	Data         interface{} // message data
}

// set message to queue
func (q *BrokerMessage) Push(msg Message) error {
	if q.shutdown {
		return ErrShutdownBroker
	}
	err := q.routeMessage(msg.ExchangeName, msg.RoutingKey, msg.Data)
	if err != nil {
		return err
	}

	return nil
}

// route message to queue exchange
func (q *BrokerMessage) routeMessage(exchangeName string, routingKey string, data interface{}) error {
	ex, ok := q.exchangesPool[exchangeName]
	if !ok {
		return errors.New("exchange don't exists")
	}

	queueName := ex.routing.keys[routingKey]
	queue, ok := q.queuePool[queueName]
	if !ok {
		return errors.New("queue don't exists")
	}
	if err := queue.Push(data); err != nil {
		return err
	}

	return nil
}

type RegisterQueue struct {
	Name string
	Size uint64
}

func (q *BrokerMessage) DeclareQueue(reg RegisterQueue) {
	q.queuePool[reg.Name] = createQueue(q.logger, reg.Name, reg.Size)
}

func (q *BrokerMessage) Pop(queueName string) (interface{}, error) {
	if data, ok := q.queuePool[queueName]; ok {
		q.countOfMessage--
		result := data.Pop()
		if result == nil && q.shutdown {
			return nil, ErrShutdownBroker
		}
		return data.Pop(), nil
	}

	return nil, nil
}

// известить что мы закрываемся
func (q *BrokerMessage) Shutdown() {
	q.shutdown = true
	q.logger.Info("queue notify shutdown")
}
