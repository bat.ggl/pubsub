package queue

import (
	"container/list"
	"go.uber.org/zap"
)

type queue struct {
	name        string
	list        *list.List
	size        uint64
	counterSize uint64
	logger      *zap.SugaredLogger
}

// create new exchange
func createQueue(logger *zap.SugaredLogger, name string, size uint64) *queue {
	return &queue{
		name:   name,
		list:   list.New(),
		size:   size,
		logger: logger,
	}
}

func (q *queue) Push(data interface{}) error {
	if q.size == q.counterSize {
		return ErrExchangeNoEnoughSize
	}
	defer func() { q.counterSize++ }()

	q.list.PushFront(data)

	return nil
}

// get first element from exchange
func (q *queue) Pop() interface{} {
	element := q.list.Front()
	if element == nil {
		return nil
	}

	q.list.Remove(element)
	q.counterSize--
	return element.Value
}
