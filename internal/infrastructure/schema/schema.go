package schema

import (
	"encoding/json"
	"gitlab.com/bat.ggl/pubsub/internal/infrastructure/validator"
	"go.uber.org/zap"
	"os"
)

type Sources struct {
	Id            string `json:"id" validate:"required"`
	InitValue     int    `json:"init_value" validate:"required"`
	MaxChangeStep int    `json:"max_change_step" validate:"required"`
}

type Generator struct {
	TimeoutInSec  int8      `json:"timeout_s" validate:"required"`
	SendPeriodSec int8      `json:"send_period_s" validate:"required"`
	DataSources   []Sources `json:"data_sources" validate:"required"`
}

type Aggregator struct {
	SubIds          []string `json:"sub_ids" validate:"required"`
	AggregatePeriod int8     `json:"aggregate_period_s" validate:"required,min=1"`
}

type SizeOfQueue struct {
	Size uint64 `json:"size" validate:"required,min=1"`
}

type Schema struct {
	Generators          []Generator  `json:"generators" validate:"required"`
	Aggregators         []Aggregator `json:"aggregators" validate:"required"`
	Queue               SizeOfQueue  `json:"queue" validate:"required"`
	AbstractStorageType int          `json:"storage_type"`
}

func LoadSchema(logger *zap.SugaredLogger, path string) *Schema {
	if len(path) == 0 {
		logger.Fatal("path can't be empty")
	}

	file, err := os.Open(path)
	if err != nil {
		logger.Panic("Couldn't load file: %s", err)
	}
	defer func() {
		err := file.Close()
		if err != nil {
			logger.Errorf("failed to close file: %s", err)
		}
	}()

	size, err := getFileSize(file)
	if err != nil {
		logger.Fatal(err)
	}

	buffer, err := readFileToBuffer(size, file)
	if err != nil {
		logger.Fatal(err)
	}

	schema := &Schema{}
	err = json.Unmarshal(buffer, schema)
	if err != nil {
		logger.Fatal(err)
	}

	if err = schema.validate(); err != nil {
		logger.Fatal(err)
	}

	logger.Info("Schema load - OK")
	return schema
}

func (s *Schema) validate() error {
	return validator.Validate(s)
}

func getFileSize(file *os.File) (int64, error) {
	stat, err := file.Stat()
	if err != nil {
		return 0, err
	}

	size := stat.Size()
	return size, nil
}

func readFileToBuffer(size int64, file *os.File) ([]byte, error) {
	buffer := make([]byte, size)
	_, err := file.Read(buffer)
	if err != nil {
		return nil, err
	}

	return buffer, nil
}
