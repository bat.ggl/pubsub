package interfaces

import (
	"flag"
	"gitlab.com/bat.ggl/pubsub/internal/application"
)

func RegisterCli(app *application.Application) {
	filepath := flag.String("filepath", "test.txt", "file storage data")
	flag.Parse()
	app.Run(*filepath)
}
